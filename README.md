# 一个不成熟的滚动区域下拉刷新指令

## 安装方法 

``` js

npm i zj-refresh

import zjRefresh from 'zj-refresh'
import 'zj-refresh/dist/style.css'
app.use(zjRefresh,下拉的内容的自定义组件 非必填 下面细说)

```

## 使用方法



``` js

//使用 v-refresh 指令给你的滚动区域添加刷新功能  v-refresh.80  可通过修饰符来自定义下拉距离

// v-refresh="refresh"  //触发刷新回调方法

import type {handleRefresh} from 'zj-refresh'

const refresh:handleRefresh = (stop)=>{

   setTimeout(()=>{

      stop(status?:boolean=true,delay?:number=1000) // 第一个参数表示刷新的成功或者失败  第二个参数默认 500 是调用stop后多久收起  也就是 刷新状态的文字显示多久 默认500

   },2000)

}

```

``` html

<div v-refresh="refresh" style="height: 300px;border: 1px solid red;overflow: scroll;">
   <div v-for="item in 1000">{{ item }}</div>
</div>

```

## 下拉内容自定义 use的第二个参数

```vue

这个是默认数据组件  自定义组件可以直接仿照这个

<template>
    <div class="zj-refresh-container">
        <div v-if="!data.packUp" style="display: flex;">
            <div v-if="data.active">刷新中</div>
            <div v-if="data.overstepDistance">释放刷新</div>
            <div v-if="!data.overstepDistance && !data.active" style="display: flex;">
                <div>下拉刷新</div>
            </div>
        </div>
        <div v-else>
            <div v-if="data.success">刷新成功</div>
            <div v-else="data.success">刷新失败</div>
        </div>
    </div>
</template>

<script lang="ts" setup>

export interface propsInterface {
    data: {
        active: boolean,   //刷新状态
        height: number,   //当前下拉高度
        clearTime: boolean,   //
        distance: boolean,   //阈值
        overstepDistance: boolean,  //当前高度是否超过阈值
        success: boolean,   //刷新状态
        packUp: boolean     //触发shop会更新为true
    }
}

defineProps<propsInterface>()

</script>

<style lang="scss" scoped>

.zj-refresh-container{
    font-size: 12px;
    color: #424242;
}

</style>


```
