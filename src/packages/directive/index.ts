import type { App,Component } from "vue";
import vRefresh from './vRefresh/index' 
import Content from './vRefresh/Content.vue'

export default{

    install(app:App,component?:Component){
        // console.log('install')
        app.directive('refresh',vRefresh({component:component||Content}))

    }

}