import { type Ref, ref, onMounted, computed } from "vue"

export function useScroll(type: 'x' | 'y' = 'y', doc: Ref<HTMLElement | undefined>) {

    let startGap = ref({
        x: 0,
        y: 0
    })

    let currntGap = ref({
        x: 0,
        y: 0
    })

    let docScroll = ref({
        x: 0,
        y: 0
    })

    const diff = computed(() => {
        return {
            x: +(startGap.value.x - currntGap.value.x).toFixed(0),
            y: +(startGap.value.y - currntGap.value.y).toFixed(0)
        }
    })


    const start = (event: TouchEvent) => {
        // console.log('start')
        // if(doc.value){
        let c = event.touches[0]

        startGap.value.x = c.pageX

        startGap.value.y = c.pageY

        currntGap.value.x = c.pageX

        currntGap.value.y = c.pageY

        docScroll.value.x = doc.value!.scrollLeft
        docScroll.value.y = doc.value!.scrollTop

        // console.log(startGap.value.x-currntGap.value.x,diff.value)

        // }

    }

    const move = (event: TouchEvent) => {
        // console.log('move')
        // if(doc.value){
        let c = event.touches[0]
        currntGap.value.x = c.pageX

        currntGap.value.y = c.pageY
        docScroll.value.x = doc.value!.scrollLeft
        docScroll.value.y = doc.value!.scrollTop

        // }

    }

    const end = (event: TouchEvent) => {

        if (doc.value) {

            docScroll.value.x = doc.value!.scrollLeft
            docScroll.value.y = doc.value!.scrollTop

            currntGap.value.x = 0

            currntGap.value.y = 0

            startGap.value.x = 0

            startGap.value.y = 0

        }

    }


    return {
        start,
        move,
        diff,
        end,
        docScroll


    }

}