import  {type Directive,ref, watch,createApp,type Component } from "vue"
import { useScroll } from "./use"
import PullDown from './PullDown.vue'



const directive = ({component}:{component:Component})=>{
    return {
        created:(el,bind)=>{

            const [distance=40] = Object.keys(bind.modifiers)

            // console.log(distance)

            const handleRefresh = bind.value

            let div = document.createElement("div")

            const scroll = useScroll('y',ref(el))

            // const showPullDown = ref(true)

            const pullDown = createApp(PullDown,{
                component,
                distance,
            })

            // console.log(pullDown.config)

            pullDown.mount(div)

            const pullDownInstance = pullDown._instance!.proxy as any

            watch(()=>scroll.diff.value.y,(v)=>{

                pullDownInstance.setHeight(v)
            })
    
    
            el.insertBefore(div,el.firstChild)
    
            el.addEventListener('scroll',(event:Event)=>{

            })
            
            el.addEventListener('touchstart',(event)=>{

                scroll.start(event)
            })
    
            el.addEventListener('touchmove',(event)=>{
                scroll.move(event)
                if(scroll.docScroll.value.y<=0&&scroll.diff.value.y<=0){
                    event.preventDefault()
                }
            })
    
            el.addEventListener('touchend',(event)=>{
                scroll.end(event)
                pullDownInstance.touchEnd(handleRefresh)
            })
    
            
        },
    } as Directive<HTMLElement,handleRefreshType>
}

export default directive