declare global {

    type stopType = (status?:boolean,delay?:number)=>void

    type handleRefreshType = (stop:stopType)=>void
   
}

export { }