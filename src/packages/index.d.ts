import './zj-refresh'
import { App, Component } from 'vue'


declare namespace _default {

    export { install };

};
export function install(app: App, component?: Component): void;

export type handleRefresh = handleRefreshType